/** @file <file_name>.c
 *  @brief Give a description of the file 
 *  
 *  Give full description of the file 
 *  
 *  @author Full name of the author 
 *  @bug List Any bugs found in the file  
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *  It typically contains Includes, constants or global variables used
 *  throughout the file.
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
/* --- Project Includes --- */


/*
 *#####################################################################
 *  Process block
 *  -------------
 *  Solve all your problems here 
 *#####################################################################
 */
 typedef struct data_given
{
    char sample[2];
    char mlii[10];
    char v5[5];
    char timestamp[50];
}data;

/** 
 *  @brief Description on function_1
 *  
 *  Full description of the function
 *
 *  @return List all Function returns 
 */

void function_1 ( data datas[])
{

        /*Make sure you comment every line */
        printf("What timestamp you want to search for:\n");
        char timestampsearch[50];
        fgets(timestampsearch,50,stdin);
        for(int i=0;i<1000;i++)
        {
            if(timestampsearch==datas[i].timestamp)
            {
                printf("sample: %s   | MLII: %s | v5: %s |",datas[i].sample,datas[i].mlii,datas[i].v5);
            }
        }
}

/** 
 *  @brief Description on main
 *  
 *  Full description of the function
 *
 *  @return List all Function returns 
 */

int main (void)
{

        /*Make sure you comment every line */
        FILE *myfile=fopen("data.csv","r");  /* oprning the file */
        char datasets[1000]; 
        if(myfile==NULL)                     // checking wheather the file is empty 
        {
                printf("EMPTY");
        }
        data datas[999];                      // array to store values 
        int i=0;
        while(fgets(datasets,1000,myfile))
        {   
                int x=0,y=0;
                if(y==1)                     // to avoid the labels like(sample,mlii)
                {
                    continue;
                }
                char *token;
                token=strtok(datasets,","); // seperating by comma
                while(token)
                {
                    if(x==0)                               // storing all values in struct format 
                        strcpy(datas[i].sample,token);
                    if(x==1)
                        strcpy(datas[i].mlii,token);
                    if(x==2)
                        strcpy(datas[i].v5,token);
                    if(x==3)
                        strcpy(datas[i].timestamp,token);
                    token=strtok(NULL,",");
                    x++;
                }
                i++;
        }
        fclose(myfile); // closing the file
        function_1(datas);    /*function calling to print the output*/
        return 0;
        
}

