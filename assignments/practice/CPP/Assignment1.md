## Problem Statement
Write a program to perform element-wise multiplication (explained below) on the 2 images(cat and dog given in extras folder) with given 3x3 filter (explained below) and display the results i.e. filtered images of Cat and Dog.

**Understanding kerwords**

1. Element-wise multiplication is also called as convolution, check below gif to understand its working.

![Convolution](/extras/conv.gif)

In above diagram filter is
```
1 0 1
0 1 0
1 0 1
```

2. Filter : Filter is usually a nxn matrix with values. In this problem statement, filter is defined as 3x3- and values are defined as below

```
1 0 0
0 1 0
0 0 1
```

### Pseudo Code
1. Create 2 classes **Cat** and **Dog**
2. Cat and Dog classes have readCatImg() and readDogImg() functions respectively
   - readCatImg() : reads an image from system in vector
   - readDogImg() : reads an image from system in vector
3. Create new class **Convolution** inherits Cat, Dog
   - Call readCatImg() function from Cat class
   - Call readDogImg() function from Dog class
   - Do element-wise multiplication (convolution) with Filter(3x3) on Cat image which is a vector
   - Do element-wise multiplication (convolution) with Filter(3x3) on Dog image which is a vector
   - Store results in vectors
   - Convert vectors to images
   - Store images of filtered Cat and Dog images in system.
