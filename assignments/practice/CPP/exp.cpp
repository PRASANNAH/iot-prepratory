#ifndef CAFFE_BASE_CONVOLUTION_LAYER_HPP_
#define CAFFE_BASE_CONVOLUTION_LAYER_HPP_

// Assignment-comment------including vector: standard template library-----------
#include <vector>

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/im2col.hpp"

// Assignment-comment------Code is under Caffe namespace-----------

namespace caffe {

/**
 * @brief Abstract base class that factors out the BLAS code common to
 *        ConvolutionLayer and DeconvolutionLayer.
 */
// Assignment-comment------creating a generic data type variable Dtype with template function-----------
template <typename Dtype>
// Assignment-comment------inheriting Layer class-----------
class BaseConvolutionLayer : public Layer<Dtype> {
// Assignment-comment------writing functions under public access modifier-----------
 public:
  explicit BaseConvolutionLayer(const LayerParameter& param)
      : Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
// Assignment-comment------Defining virtual inline function-----------
  virtual inline int MinBottomBlobs() const { return 1; }

// Assignment-comment------writing functions under protected access modifier-----------
 protected:
  // Helper functions that abstract away the column buffer and gemm arguments.
  // The last argument in forward_cpu_gemm is so that we can skip the im2col if
  // we just called weight_cpu_gemm with the same input.
  void forward_cpu_gemm(const Dtype* input, const Dtype* weights,
  
#endif  